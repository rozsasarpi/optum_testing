# optum_testing

A repo for testing optum features and sharing models.

All codes in this repository is expected to be run with the working directory set as the root directory of the repository: `\optum_testing\`

Tested on:
 - Windows 10
 - conda environment with python 3.7.4