# This file reads G2 input files using xml parsers and writes a new xml file without any modifications
# if the "deltares_spw_template.g2x" model file is used then the new model has no boundary conditions along its edges
#
# for testing the differences between files: https://www.diffchecker.com/diff

import os
import xml.etree.ElementTree as ET
from lxml import etree as lET

model_dir = "FE_models"
model_name = "deltares_spw_template.g2x"

model_path = os.path.join(model_dir, model_name)

# --------------------------------------------------------------
# XML package
# --------------------------------------------------------------

# Read
with open(model_path, encoding="utf-8") as file:
    model_tree = ET.parse(file)

# Write
model_copy_path = model_path[:-4] + "_xml copy" + model_path[-4:]
model_tree.write(model_copy_path, xml_declaration=True, method='xml', encoding="utf-8")

# --------------------------------------------------------------
# LXML package
# --------------------------------------------------------------
# Read
with open(model_path, encoding="utf-8") as file:
    model_tree = lET.parse(file)

# Write
model_copy_path = model_path[:-4] + "_lxml copy" + model_path[-4:]
model_tree.write(model_copy_path, xml_declaration=True, method='xml', encoding="utf-8")
